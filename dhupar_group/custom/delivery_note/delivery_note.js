frappe.ui.form.on("Delivery Note", {
	onload: function(frm){
		if(frm.doc.customer_reservation_type == "One Time In Full Reservation (OTFR)"){
			 var qty = frappe.meta.get_docfield('Delivery Note Item', "qty", frm.doc.name)
			 qty.read_only = 0;
			 var item_code = frappe.meta.get_docfield('Delivery Note Item', "item_code", frm.doc.name)
			 item_code.read_only = 1;
			 var item_name = frappe.meta.get_docfield('Delivery Note Item', "item_name", frm.doc.name)
			 item_name.read_only = 1;
			 var rate = frappe.meta.get_docfield('Delivery Note Item', "rate", frm.doc.name)
			 rate.read_only = 1;
			 var warehouse = frappe.meta.get_docfield('Delivery Note Item', "warehouse", frm.doc.name)
			 warehouse.read_only = 1;

			 frm.set_df_property('set_warehouse', "read_only", 1);
			 frm.set_df_property('customer', "read_only", 1);
			 frm.set_df_property('emergency_dispatch', 'read_only', 1);
		}
		else if (frm.doc.customer_reservation_type == "Partial As per Available Reservation (PAAR)"){
			var qty = frappe.meta.get_docfield('Delivery Note Item', "qty", frm.doc.name)
			qty.read_only = 0;
			var item_code = frappe.meta.get_docfield('Delivery Note Item', "item_code", frm.doc.name)
			item_code.read_only = 1;
			var item_name = frappe.meta.get_docfield('Delivery Note Item', "item_name", frm.doc.name)
			item_name.read_only = 1;
			var rate = frappe.meta.get_docfield('Delivery Note Item', "rate", frm.doc.name)
			rate.read_only = 1;
			var warehouse = frappe.meta.get_docfield('Delivery Note Item', "warehouse", frm.doc.name)
			warehouse.read_only = 1;

			frm.set_df_property('set_warehouse', "read_only", 1);
			frm.set_df_property('customer', "read_only", 1);
			frm.set_df_property('emergency_dispatch', 'read_only', 1);


			
		}
	}
})