from __future__ import unicode_literals
import frappe
from frappe.utils import nowdate, cstr, flt

@frappe.whitelist()
def validate(self,method=None):
	update_emergeny_dispatch(self)
	reserved_qty_validation(self)
	# get_picklist_entries(self)

@frappe.whitelist()
def on_submit(self,method=None):
	update_delivered_flag(self)
	get_picklist_entries(self)
	normal_picklist(self)
	update_rs_qtys(self)
	update_delivered_se_qty(self)
	update_reservation_shedule_qty(self)

@frappe.whitelist()
def on_cancel(self,method=None):
	cancel_rs_quantity(self)
	update_cancelled_reservation_shedule_qty(self)

@frappe.whitelist()
def on_update_after_submit(self,method=None):
	pass
	

""" Update Delivery flag on Stock Entry"""
@frappe.whitelist()
def update_delivered_flag(self,method=None):
	if self.reservation_schedule_ref_no:
		frappe.db.set_value("Stock Entry", {"reservation_schedule_ref_no":self.get('reservation_schedule_ref_no')}, "is_delivered",1)
		frappe.db.commit()

""" Update quantities for reservation schdule with reference"""
@frappe.whitelist()
def update_rs_qtys(self,method=None):
	if self.reservation_schedule_ref_no:
		res_doc = frappe.get_doc("Reservation Schedule", self.reservation_schedule_ref_no)
		for item in self.items:
			for res_item in res_doc.items:
				if res_item.item_code == item.item_code and res_item.name == item.reservation_schedule_item_name:
					res_item.delivered_qty = res_item.delivered_qty + item.qty
					res_item.reserved_qty = res_item.reserved_qty - item.qty
		res_doc.save()

""" Update cancelled quantities for reservation schdule with reference"""
@frappe.whitelist()
def cancel_rs_quantity(self,method=None):
	if self.reservation_schedule_ref_no:
		res_doc = frappe.get_doc("Reservation Schedule", self.reservation_schedule_ref_no)
		for item in self.items:
			for res_item in res_doc.items:
				if res_item.item_code == item.item_code and res_item.name == item.reservation_schedule_item_name:
					res_item.delivered_qty = res_item.delivered_qty - item.qty
					res_item.remaining_qty = res_item.remaining_qty + item.qty
		res_doc.save()

""" Quantity to be Delivered cannot be greater than Reserved qty"""
@frappe.whitelist()
def reserved_qty_validation(self, method=None):
	for row in self.items:
		if row.against_reservation_schedule:
			if row.qty > row.allowed_qty:
				frappe.throw("Order quantity cannot be greater than Allowed quantity.")

""" emergency dispatch check box update"""
@frappe.whitelist()
def update_emergeny_dispatch(self):

	res_warehouse = frappe.db.sql("""
			SELECT name from `tabWarehouse` where reservation_warehouse = 1
		""",as_dict=1)
	res_warehouse_list = list(set([res.name for res in res_warehouse]))
	
	if not self.reservation_schedule_ref_no:
		for item in self.items:
			if self.set_warehouse in res_warehouse_list or item.warehouse in res_warehouse_list:
				self.emergency_dispatch = 1

""" Pick List for RS Reference"""
@frappe.whitelist()
def get_picklist_entries(self):
	if self.reservation_schedule_ref_no:

		reserved_stock_entries_with_item = frappe.db.sql("""
			SELECT se.name as se_name,se.creation,sed.name as sed_name,sed.uom,sed.item_name,se.reservation_schedule_ref_no,sed.s_warehouse,sed.item_code,sed.t_warehouse,sed.qty,sed.delivered_qty,sed.reservation_schedule_item,(sed.qty -sed.delivered_qty) as remaining_qty 
				from `tabStock Entry` as se ,`tabStock Entry Detail` as sed 
				where se.name = sed.parent and se.reservation_schedule_ref_no = '{0}' and se.docstatus = 1 order by se.creation desc""".format(self.reservation_schedule_ref_no),as_dict=1)

		if reserved_stock_entries_with_item:
			for item in self.items:
				stock_qty = item.qty
				for stock in reserved_stock_entries_with_item:
					if stock.item_code == item.item_code and stock.reservation_schedule_item == item.reservation_schedule_item_name and stock_qty > 0:
						if stock_qty > stock.remaining_qty:
							self.append("pick_up_list",{
								"source_warehouse":stock.s_warehouse,
								"item_code": stock.item_code,
								"item_name": stock.item_name,
								"quantity": stock.remaining_qty,
								"target_warehouse": stock.t_warehouse,
								"stock_entry_name":stock.se_name,
								"UOM":stock.uom,
								"reservation_schedule":stock.reservation_schedule_ref_no,
								"delivery_qty":item.qty,
								"stock_entry_item_name": stock.sed_name,
								"reservation_item": stock.reservation_schedule_item
							})
							stock_qty = stock_qty - stock.remaining_qty
						elif stock_qty <= stock.remaining_qty:
							self.append("pick_up_list",{
								"source_warehouse":stock.s_warehouse,
								"item_code": stock.item_code,
								"quantity": stock_qty,
								"target_warehouse": stock.t_warehouse,
								"stock_entry_name":stock.se_name,
								"item_name": stock.item_name,
								"UOM":stock.uom,
								"reservation_schedule":stock.reservation_schedule_ref_no,
								"delivery_qty":item.qty,
								"stock_entry_item_name": stock.sed_name,
								"reservation_item": stock.reservation_schedule_item

							})
							stock_qty = stock_qty - stock.remaining_qty
				self.save()

""" Picklist for emergency dispatch"""
@frappe.whitelist()
def normal_picklist(self):
	if not self.reservation_schedule_ref_no:
		if self.emergency_dispatch == 1:
			for item in self.items:
				reserved_stock_entries_with_item = frappe.db.sql("""
					SELECT se.name as se_name,se.creation,se.reservation_schedule_ref_no,sed.name,sed.uom,sed.item_name,sed.s_warehouse,sed.item_code,sed.reservation_schedule_item,sed.t_warehouse,sed.qty,sed.delivered_qty,(sed.qty -sed.delivered_qty) as remaining_qty 
						from `tabStock Entry` as se ,`tabStock Entry Detail` as sed 
						where se.name = sed.parent and se.stock_status in ('Open','Partial') and se.reservation_schedule_ref_no is NOT NULL and se.docstatus = 1 and sed.t_warehouse = '{0}' order by se.creation desc""".format(item.warehouse),as_dict=1)
				available_qty_list = [ flt(stock_entries.get('remaining_qty')) for stock_entries in reserved_stock_entries_with_item if flt(stock_entries.get('remaining_qty')) ]
				available_qty = sum(available_qty_list)
				if item.qty < available_qty:
					if reserved_stock_entries_with_item:
						for item in self.items:
							stock_qty = item.qty
							for stock in reserved_stock_entries_with_item:
								if stock.item_code == item.item_code and stock_qty > 0:
									if stock_qty <= stock.remaining_qty:
										self.append("pick_up_list",{
											"source_warehouse":stock.s_warehouse,
											"item_code": stock.item_code,
											"quantity": stock_qty,
											"target_warehouse": stock.t_warehouse,
											"stock_entry_name":stock.se_name,
											"item_name": stock.item_name,
											"UOM":stock.uom,
											"reservation_schedule":stock.reservation_schedule_ref_no,
											"delivery_qty":item.qty,
											"reservation_item":stock.reservation_schedule_item,
											"stock_entry_item_name": stock.name
										})
										stock_qty = stock_qty - stock.remaining_qty
									elif stock_qty >= item.qty:
										self.append("pick_up_list",{
											"source_warehouse":stock.s_warehouse,
											"item_code": stock.item_code,
											"item_name": stock.item_name,
											"quantity": stock.remaining_qty,
											"target_warehouse": stock.t_warehouse,
											"stock_entry_name":stock.se_name,
											"UOM":stock.uom,
											"reservation_schedule":stock.reservation_schedule_ref_no,
											"delivery_qty":item.qty,
											"reservation_item":stock.reservation_schedule_item,
											"stock_entry_item_name": stock.name
										})
										stock_qty = stock_qty - stock.remaining_qty
				else:
					frappe.throw("Reservation Warehouse does not have enough quantity to book this Delivery Note")
				self.save()
				for row in self.pick_up_list:
					"""Rs Update"""
					rs_data = frappe.db.get_values("Reservation Schedule Item",row.reservation_item,['remaining_qty','reserved_qty'],as_dict=1)
					if rs_data:
						remaining_qty = rs_data[0].get('remaining_qty')
						remaining_qty = remaining_qty + row.quantity
						reserved_qty = rs_data[0].get('reserved_qty')
						reserved_qty = reserved_qty - row.quantity
						frappe.db.set_value("Reservation Schedule Item",row.reservation_item,'remaining_qty',remaining_qty)
						frappe.db.set_value("Reservation Schedule Item",row.reservation_item,'reserved_qty',reserved_qty)
						frappe.db.set_value("Reservation Schedule",row.reservation_schedule,"reservation_status","Partial")
						frappe.db.commit()
					update_is_used_flag(self,row.stock_entry_name)

""" Update Flag on Stock Entry"""
@frappe.whitelist()
def update_is_used_flag(self,stock_entry_name):
	if not self.reservation_schedule_ref_no:
		if self.emergency_dispatch == 1:
			frappe.db.set_value("Stock Entry", {"name":stock_entry_name}, "is_used",1)
			frappe.db.commit()
							
""" Delivered qty on Stock entry for RS reference"""
@frappe.whitelist()
def update_delivered_se_qty(self):
	if self.reservation_schedule_ref_no:
		for pick_list_item in self.pick_up_list:
			stock_doc = frappe.get_doc("Stock Entry",{"name":pick_list_item.stock_entry_name})
			for item in stock_doc.items:
				if pick_list_item.item_code == item.item_code and pick_list_item.stock_entry_item_name == item.name and pick_list_item.reservation_item == item.reservation_schedule_item:
					item.delivered_qty = item.delivered_qty + pick_list_item.quantity
			stock_doc.save()
			update_stock_status(pick_list_item.stock_entry_name)
	else:
		if self.emergency_dispatch == 1:
			for pick_list_item in self.pick_up_list:
				stock_doc = frappe.get_doc("Stock Entry",{"name":pick_list_item.stock_entry_name})
				for item in stock_doc.items:
					if item.item_code == pick_list_item.item_code and item.name == pick_list_item.stock_entry_item_name:
						item.delivered_qty = item.delivered_qty + pick_list_item.quantity
				stock_doc.save()
				update_stock_status(pick_list_item.stock_entry_name)

@frappe.whitelist()
def update_stock_status(stock_entry_name):
	se_doc = frappe.get_doc("Stock Entry", stock_entry_name)
	delivery_qty = 0.0
	total_qty = 0.0
	for item in se_doc.items:
		delivery_qty = flt(delivery_qty) + flt(item.delivered_qty)
		total_qty = flt(total_qty) + flt(item.qty)
	if delivery_qty > 0 and delivery_qty < total_qty:
		frappe.db.set_value("Stock Entry", stock_entry_name, "stock_status", "Partial")
	elif delivery_qty == total_qty:
		frappe.db.set_value("Stock Entry", stock_entry_name, "stock_status", "Completed")
	elif delivery_qty == 0.0:
		frappe.db.set_value("Stock Entry", stock_entry_name, "stock_status", "Open")

""" Update reservation Schedule qty on Sales order """
@frappe.whitelist()
def update_reservation_shedule_qty(self):
	if not self.reservation_schedule_ref_no:
		if self.emergency_dispatch == 0:
			for item in self.items:
				if item.against_sales_order:
					so_doc = frappe.get_doc("Sales Order", {"name":item.against_sales_order})
					for sale in so_doc.items:
						if item.item_code == sale.item_code:
							sale.reservation_schedule_qty = sale.reservation_schedule_qty - item.qty
					so_doc.save()

""" Update cancelled delivery note qty on reservation Schedule qty on Sales order """
@frappe.whitelist()
def update_cancelled_reservation_shedule_qty(self):
	if not self.reservation_schedule_ref_no:
		if self.emergency_dispatch == 0:
			for item in self.items:
				if item.against_sales_order:
					so_doc = frappe.get_doc("Sales Order", {"name":item.against_sales_order})
					for sale in so_doc.items:
						if item.item_code == sale.item_code:
							sale.reservation_schedule_qty = sale.reservation_schedule_qty + item.qty
					so_doc.save()
