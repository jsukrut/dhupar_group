frappe.ui.form.on("Sales Order", {
	refresh: function(frm){
		frm.trigger('querys_and_filters')
		frm.trigger('add_custom_button_make')
		
		if (frm.doc.reserv_against_order == 1){
			setTimeout(() => {
        		frm.remove_custom_button('Delivery', 'Make');
        	}, 10);
		}
		if (frm.doc.reserv_against_order == 1){
			setTimeout(() => {
        		frm.remove_custom_button('Invoice', 'Make');
        	}, 10);
		}


	},

	onload: function(frm){
		frm.toggle_display('group_warehouse', true);
		frm.toggle_display('reserved_warehouse', true);
		frm.toggle_display("reserved_days", true);
		frm.trigger('set_so_property')
		frm.trigger('status')
	},
	on_submit: function(frm){
		frm.trigger('set_so_property')
	},


	set_so_property:function(frm){
		if ((frm.doc.reserv_against_order == 1) && (frm.doc.docstatus == 1)){
			frm.set_df_property('reserv_against_order', 'read_only', 1);
			frm.set_df_property('customer_reservation_type', 'read_only', 1);
			frm.set_df_property('group_warehouse', 'read_only', 1);
		}
	},

	add_custom_button_make: function(frm){
		if((frm.doc.docstatus == 1 && frm.doc.reserv_against_order) && (frm.doc.customer_reservation_type == "Partial As per Available Reservation (PAAR)")) {
			frm.add_custom_button(__("Reservation Schedule"),function() {
				frappe.call({
					method: "dhupar_group.custom.sales_order.sales_order.make_reservation_schedule", 
					args:{
						"doc":cur_frm.doc
					},
					async: false,
					callback: function(r) {
						if(r.message) {
							frappe.set_route("Form", "Reservation Schedule",r.message);
						}
					}
				})
			},"Make")
			frm.page.set_inner_btn_group_as_primary(__("Make"));
		};

	},

	customer: function(frm){
		if(frm.doc.customer && frm.doc.reserv_against_order ){
			frm.set_df_property('group_warehouse', "hidden", 0);
			frm.set_df_property('reserved_warehouse', "hidden", 0);
			frm.set_df_property('reserved_days', "hidden", 0);
			frm.set_df_property('group_warehouse', "reqd", 1);
			frm.set_df_property('customer_reservation_type', "reqd", 1);
			frm.refresh_field('group_warehouse')
			frm.trigger('get_no_reservation_days')
			frm.refresh_field('customer_reservation_type')
		}else{
			frm.set_df_property('group_warehouse', "hidden", 1);
			frm.set_df_property('reserved_warehouse', "hidden", 1);
			frm.set_df_property('reserved_days', "hidden", 1);
			frm.set_df_property('group_warehouse', "reqd", 0);
			frm.set_df_property('customer_reservation_type', "reqd", 0);
			frm.set_value('group_warehouse', '')
			frm.set_value('customer_reservation_type', '')
			frm.set_value('reserved_warehouse', '')
			frm.toggle_display("reserved_days", false)
			frm.refresh_field('group_warehouse')
		}
		
		frm.trigger('default_warehouse_fetch')
		

	},

	status:function(frm){
		if(frm.doc.status == 'Closed'){
			setTimeout(() => {
				frm.remove_custom_button('Re-open', 'Status');
				frm.remove_custom_button('Reservation Schedule','Make');
			},10);
		}
	},

	reserv_against_order: function(frm){
		if(frm.doc.reserv_against_order){
			frm.set_df_property('group_warehouse', "hidden", 0);
			frm.set_df_property('reserved_warehouse', "hidden", 0);
			frm.set_df_property('reserved_days', "hidden", 0);
			frm.set_df_property('group_warehouse', "reqd", 1);
			frm.set_df_property('customer_reservation_type', "reqd", 1);
			frm.set_df_property('customer_reservation_type', "hidden", 0);
			frm.refresh_field('group_warehouse')
			frm.refresh_field('customer_reservation_type')
			// frm.trigger('default_warehouse_fetch')
		}else{
			frm.set_df_property('group_warehouse', "hidden", 1);
			frm.set_df_property('reserved_warehouse', "hidden", 1);
			frm.set_df_property('reserved_days', "hidden", 1);
			frm.set_df_property('group_warehouse', "reqd", 0);
			frm.set_df_property('customer_reservation_type', "reqd", 0);
			frm.set_df_property('customer_reservation_type', "hidden", 1);
			frm.set_value('group_warehouse', '')
			frm.set_value('reserved_warehouse', '')
			frm.set_value('customer_reservation_type', '')
			frm.refresh_field('group_warehouse')
			frm.set_value('set_warehouse','')
		}
	},

	querys_and_filters:function(frm){
		frm.set_query("group_warehouse", function() {
		    return {
		    		"query": "dhupar_group.custom.sales_order.sales_order.fetch_group_warehouse_so",
					"filters":{
						is_group: 1,
					}
				}
		});	
	},

	get_customer: function(frm){
			if(frm.doc.customer){
				frappe.call({
					method: "dhupar_group.custom.sales_order.sales_order.customer_on_reservation_configurations",
					args: {
						"customer": frm.doc.customer
					},
					callback: function(r) {
						if (r.message){
							if(frm.doc.customer == r.message){
								var OTFR = "One Time In Full Reservation (OTFR)"
								frm.set_value("customer_reservation_type", OTFR);
							}
						}
						else{
							var PAAR = "Partial As per Available Reservation (PAAR)"
							frm.set_value("customer_reservation_type", PAAR);	
						}
					}
				})
			}
	},

	get_no_reservation_days: function(frm){
			if(frm.doc.customer){
				frm.toggle_display('reserved_days', true);
				frappe.call({
					method: "frappe.client.get_value",
					args: {
						doctype: "Reservation Configurations",
						fieldname: 'reserved_days',
					},
					async:true,
					callback: function(r) {
						if(r.message){
							var reserved_no_days = parseInt(r.message.reserved_days)
							frm.set_value("reserved_days", reserved_no_days);	
						}
					}
				})
			}else{
				frm.toggle_display('reserved_days', false);
			}		
	},	


	default_warehouse_fetch: function(frm){
			if(frm.doc.reserv_against_order){
				
				frappe.call({
					method: "frappe.client.get_value",
					args: {
						doctype: "Reservation Configurations",
						fieldname: 'def_reservation_warehouse',
					},
					async:true,
					callback: function(r) {
						if(r.message){
							if(frm.doc.reserv_against_order){
								frm.set_value("set_warehouse",r.message.def_reservation_warehouse);
							}
						}
					}
				})
			}
	},	

	group_warehouse: function(frm){
		if(frm.doc.group_warehouse){
			frm.trigger('get_group_warehouse_so')
			$.each(frm.doc.items,function(idx,data){
				frappe.call({
					method: "dhupar_group.custom.sales_order.sales_order.res_warehouse_on_reservation_configurations",
					args: {
						"group_warehouse_so": frm.doc.group_warehouse
					},
					// callback: function(r) {
					// 	if (r.message){
					// 		if(frm.doc.group_warehouse){
					// 			frappe.model.set_value(data.doctype,data.name,"warehouse",r.message[0].reserved_warehouse);
					// 		}
					// 	}
					// }
				})
			})
		}
	},

	get_group_warehouse_so: function(frm){
			if(frm.doc.group_warehouse){
				frappe.call({
					method: "dhupar_group.custom.sales_order.sales_order.res_warehouse_on_reservation_configurations",
					args: {
						"group_warehouse_so": frm.doc.group_warehouse
					},
					callback: function(r) {
						if (r.message){
							if(frm.doc.group_warehouse){
								frm.set_value("reserved_warehouse", r.message[0].reserved_warehouse);
							}
						}
					}
				})
			}
	},


	def_res_warehouse_function:function(frm,row){
			var def_warehouse = frm.doc.default_warehouse;
			$.each(frm.doc.items,function(idx,data){
				if(frm.doc.customer){
					frappe.model.set_value(data.doctype,data.name,"warehouse",def_warehouse);
				}else{
					frappe.model.set_value(data.doctype, data.name, "warehouse", '')
				}
			})
		},
});


frappe.ui.form.on("Sales Order Item",{
	item_code:function(frm,cdt,cdn){
		var data = locals[cdt][cdn]
	},
});