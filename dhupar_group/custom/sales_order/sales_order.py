from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.utils import cstr, cint, getdate,flt
from frappe.utils import get_url_to_form, get_url, today, add_days
from datetime import datetime
import time
from datetime import date, timedelta
from frappe.model.mapper import get_mapped_doc
from dhupar_group.dhupar_group.doctype.reservation_schedule.reservation_schedule import reservation_shedular
import json


def validate(doc,method):
	update_rs_qty(doc)
	pass

def on_submit(doc,method):
	create_reservation_schedule(doc)

def on_change(doc,method):
	find_reservation_schedule(doc)

def on_update_after_submit(doc,method):
	find_reservation_schedule(doc)

	pass

@frappe.whitelist()
def create_reservation_schedule(doc):
	if doc.reserv_against_order == 1 and doc.customer_reservation_type == "One Time In Full Reservation (OTFR)":
		current_date = frappe.utils.today()
		reservation_schedule = frappe.new_doc("Reservation Schedule")
		if reservation_schedule:
			reservation_schedule.date = current_date
			reservation_schedule.so_reference_no = doc.name
			reservation_schedule.reserved_warehouse = doc.reserved_warehouse
			reservation_schedule.group_warehouse = doc.group_warehouse
			reservation_schedule.customer = doc.customer
			reservation_schedule.customer_reservation_type = doc.customer_reservation_type
			reservation_schedule.reserved_days = doc.reserved_days
			for row in doc.items:
				is_stock = frappe.db.get_value("Item",{"name":row.item_code},"is_stock_item")
				if is_stock == 1:
					reservation_schedule.append("items",{
						"item_code": row.item_code,
						"item_name": row.item_name,
						"qty": row.qty,
						"rate": row.rate,
						"amount": row.amount,
						"item_name": row.item_name,
						"description": row.description,
						"uom": row.uom,
						"stock_uom": row.stock_uom,
						"delivery_date": row.delivery_date,
						"reservation_schedule_qty": row.reservation_schedule_qty,
						"against_sales_order": doc.name,
						"sales_order_item": row.name,
						"price_list_rate": row.price_list_rate,
						"discount_percentage": row.discount_percentage,
						"discount_amount": row.discount_amount,
						"conversion_factor": row.conversion_factor,
						"stock_qty": row.stock_qty
					})
				else:
					pass
			reservation_schedule.save()
			reservation_schedule.submit()
	
@frappe.whitelist()
def customer_on_reservation_configurations(customer):
	"""Customer fetch on Reservation Configurations Page and compare SO customer"""
	
	if customer:
		customer_name = frappe.get_value("OTFR Reservation",{"customer":customer},"customer")
		if customer_name:
			return customer_name

@frappe.whitelist()
def filter_reservation_warehouse(doctype, txt, searchfield, start, page_len, filters):
	"""Group Warehouse Against filter reserved warehouse"""
	
	if filters['reserv_against_order']:
		return frappe.db.get_value("Warehouse", {'is_group': 1}, "name")
		
@frappe.whitelist()
def fetch_group_warehouse_so(doctype, txt, searchfield, start, page_len, filters):
	"""Fetch Group Warehouse on Reservation Configurations doc"""

	return frappe.db.sql("""select group_warehouse from `tabWarehouse Configurations`""")
	 
@frappe.whitelist()
def res_warehouse_on_reservation_configurations(group_warehouse_so):
	"""Customer fetch on Reservation Configurations Page and compare SO customer"""
	
	if group_warehouse_so:
		warehouse_name = frappe.db.sql("""select reserved_warehouse from `tabWarehouse Configurations` where group_warehouse = '{0}'""".format(group_warehouse_so),as_dict = True)
		if warehouse_name:
			return warehouse_name


@frappe.whitelist()
def make_reservation_schedule(doc):
	doc = json.loads(doc)
	if doc.get('reserv_against_order') == 1 and doc.get('customer_reservation_type') == "Partial As per Available Reservation (PAAR)":
		current_date = frappe.utils.today()
		reservation_schedule = frappe.new_doc("Reservation Schedule")
		if reservation_schedule:
			reservation_schedule.date = current_date
			reservation_schedule.so_reference_no = doc.get('name')
			reservation_schedule.reserved_warehouse = doc.get('reserved_warehouse')
			reservation_schedule.group_warehouse = doc.get('group_warehouse')
			reservation_schedule.customer = doc.get('customer')
			reservation_schedule.customer_reservation_type = doc.get('customer_reservation_type')
			reservation_schedule.reserved_days = doc.get('reserved_days')
			for row in doc.get('items'):
				is_stock = frappe.db.get_value("Item",{"name":row.get('item_code')},"is_stock_item")
				if is_stock == 1:
					reservation_schedule.append("items",{
						"item_code": row.get('item_code'),
						"item_name": row.get('item_name'),
						"qty": row.get('reservation_schedule_qty'),
						"rate": row.get('rate'),
						"item_name": row.get('item_name'),
						"description": row.get('description'),
						"uom": row.get('uom'),
						"stock_uom": row.get('stock_uom'),
						"delivery_date": row.get('delivery_date'),
						"reservation_schedule_qty": row.get('reservation_schedule_qty'),
						"against_sales_order": doc.get('name'),
						"sales_order_item": row.get('name'),
						"price_list_rate": row.get('price_list_rate'),
						"discount_percentage": row.get('discount_percentage'),
						"discount_amount": row.get('discount_amount'),
						"conversion_factor": row.get('conversion_factor'),
						"stock_qty": row.get('stock_qty')
					})
				else:
					pass
			reservation_schedule.save()
	return reservation_schedule.name

@frappe.whitelist()
def update_rs_qty(doc):
	if doc.docstatus == 1:
		for item in doc.items:
			item.reservation_schedule_qty = item.qty


@frappe.whitelist()
def find_reservation_schedule(doc):
	if (doc.status == 'Closed'):
		rs_doc = frappe.db.sql("""
			UPDATE `tabReservation Schedule` SET reservation_status = 'Closed' where so_reference_no = "{0}"
			""".format(doc.name),as_dict=1)

