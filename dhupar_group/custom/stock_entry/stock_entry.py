from __future__ import unicode_literals
import frappe
from frappe.utils import nowdate, cstr, flt, cint, now, getdate,get_datetime,time_diff_in_seconds,add_to_date,time_diff_in_seconds,add_days
	
@frappe.whitelist()
def on_submit(self,method=None):
	stock_entries_for_scrap(self)
	pass
	
@frappe.whitelist()
def on_cancel(self,method=None):
	update_cancel_rs_qty(self)
	cancel_validation(self)

@frappe.whitelist()
def validate(self,method=None):
	warehouse_validation(self)
	material_transfer_validation(self)
	pass

@frappe.whitelist()
def on_update_after_submit(self,method=None):
	update_stock_status(self)
	pass


""" Warehouse validation for Scrap or Movement"""
@frappe.whitelist()
def warehouse_validation(self):
	res_warehouse = frappe.db.sql("""
			SELECT name from `tabWarehouse` where reservation_warehouse = 1
		""",as_dict=1)
	res_warehouse_list = list(set([res.name for res in res_warehouse]))

	if not self.reservation_schedule_ref_no:
		if self.purpose == "Material Transfer":
			if self.is_scrap == 0:
				for item in self.items:
					if self.from_warehouse in res_warehouse_list or item.s_warehouse in res_warehouse_list and self.stock_return_dn == 0:
						self.is_scrap = 1
					elif item.t_warehouse in res_warehouse_list:
						frappe.throw("Target Warehouse cannot be Reserved warehouse if Scrap Movement is not checked.")
		elif self.purpose == "Material Issue":
			for item in self.items:
				if self.from_warehouse in res_warehouse_list or item.s_warehouse in res_warehouse_list:
					self.is_scrap = 1
				elif self.from_warehouse not in res_warehouse_list:
					self.is_scrap = 0

"""Update rs quantities on cancellation of stock entry"""
@frappe.whitelist()
def update_cancel_rs_qty(self):
	if self.reservation_schedule_ref_no:
		reserve_doc = frappe.get_doc("Reservation Schedule",{"name":self.reservation_schedule_ref_no})

		for item in self.items:
			for row in reserve_doc.items:
				if (item.item_code == row.item_code):
					row.remaining_qty = row.remaining_qty + item.qty
					row.reserved_qty = row.reserved_qty - item.qty
		reserve_doc.save()

"""Warehouse validation for material reciept for reserved warehouse"""
@frappe.whitelist()
def material_transfer_validation(self):
	res_warehouse = frappe.db.sql("""
			SELECT name from `tabWarehouse` where reservation_warehouse = 1
		""",as_dict=1)
	res_warehouse_list = list(set([res.name for res in res_warehouse]))

	if not self.reservation_schedule_ref_no:
		if self.purpose == "Material Receipt":
			for item in self.items:
				if item.t_warehouse in res_warehouse_list:
					frappe.throw("Warehouse cannot be Reserved warehouse for Material Receipt.")

"""Emergency Scrap or Movement"""
@frappe.whitelist()
def stock_entries_for_scrap(self):
	if not self.reservation_schedule_ref_no and self.is_scrap == 1 and self.stock_returned == 0 and self.stock_return_dn == 0:
		for item in self.items:
			rs_stock_entries = has_reservation_se(self,item)
			if rs_stock_entries:
				available_qty_list = [ flt(stock_entries.get('remaining_qty')) for stock_entries in rs_stock_entries if flt(stock_entries.get('remaining_qty')) ]
				available_qty = sum(available_qty_list)
				if item.qty < available_qty:
					stock_qty = item.qty 
					for stock_entry in rs_stock_entries:
						if item.s_warehouse == stock_entry.t_warehouse and item.item_code == stock_entry.item_code:
							if stock_qty <= stock_entry.remaining_qty and (stock_qty > 0 and stock_entry.remaining_qty >0):
								self.append("reservation_table",{
									"source_warehouse":stock_entry.s_warehouse,
									"item_code":stock_entry.item_code,
									"item_name":stock_entry.item_name,
									"quantity":stock_qty,
									"target_warehouse":stock_entry.t_warehouse,
									"stock_entry_name":stock_entry.se_name,
									"stock_entry_item":stock_entry.name,
									"uom":stock_entry.uom,
									"reservation_schedule":stock_entry.reservation_schedule_ref_no,
									"reservtion_schedule_item":stock_entry.reservation_schedule_item
								})
								stock_qty = stock_qty - stock_entry.remaining_qty
							elif stock_qty > stock_entry.remaining_qty and (stock_qty > 0 and stock_entry.remaining_qty >0):
								self.append("reservation_table",{
									"source_warehouse":stock_entry.s_warehouse,
									"item_code":stock_entry.item_code,
									"item_name":stock_entry.item_name,
									"quantity":stock_entry.remaining_qty,
									"target_warehouse":stock_entry.t_warehouse,
									"stock_entry_name":stock_entry.se_name,
									"stock_entry_item":stock_entry.name,
									"uom":stock_entry.uom,
									"reservation_schedule":stock_entry.reservation_schedule_ref_no,
									"reservtion_schedule_item":stock_entry.reservation_schedule_item
								})
								stock_qty = stock_qty - stock_entry.remaining_qty
				else:
					pass
			else:
				frappe.throw("Reservation Warehouse does not have enough quantity to book this Stock Entry")
		
		self.save()
		for row in self.reservation_table:
			''' RS Update'''
			rs_data = frappe.db.get_values("Reservation Schedule Item",row.reservtion_schedule_item,['remaining_qty','reserved_qty'],as_dict=1)
			if rs_data:
				remaining_qty = rs_data[0].get('remaining_qty')
				remaining_qty = remaining_qty + row.quantity
				reserved_qty = rs_data[0].get('reserved_qty')
				reserved_qty = reserved_qty - row.quantity
				frappe.db.set_value("Reservation Schedule Item",row.reservtion_schedule_item,'remaining_qty',remaining_qty)
				frappe.db.set_value("Reservation Schedule Item",row.reservtion_schedule_item,'reserved_qty',reserved_qty)
				frappe.db.set_value("Reservation Schedule",row.reservation_schedule,"reservation_status","Partial")
			""" SE Update"""
			se_data = frappe.db.get_values("Stock Entry Detail",row.stock_entry_item,['delivered_qty'],as_dict=1)
			if se_data:
				delivered_qty = se_data[0].get("delivered_qty")
				delivered_qty = delivered_qty + row.quantity
				frappe.db.set_value("Stock Entry Detail",row.stock_entry_item,'delivered_qty',delivered_qty)
		frappe.db.commit()

"""SQL Query for Scrap or movement"""
@frappe.whitelist()
def has_reservation_se(doc,item):

	scrap_limit = frappe.db.sql("""SELECT se.name as se_name,se.creation,se.reservation_schedule_ref_no,sed.reservation_schedule_item,sed.name,sed.uom,sed.item_name,sed.s_warehouse,sed.item_code,sed.t_warehouse,sed.qty,sed.delivered_qty,(sed.qty -sed.delivered_qty) as remaining_qty
		from `tabStock Entry` as se ,`tabStock Entry Detail` as sed 
		where se.name = sed.parent and se.stock_status in ('Open','Partial') and se.reservation_schedule_ref_no is NOT NULL and se.docstatus = 1 and sed.t_warehouse = '{0}' and sed.item_code = '{1}'order by se.creation desc """.format(item.s_warehouse,item.item_code),as_dict=1)
	if scrap_limit:
		return scrap_limit

"""Update Stock Status on Stock Entry"""
@frappe.whitelist()
def update_stock_status(self):
	if self.reservation_schedule_ref_no:
		for item in self.items:
			if item.delivered_qty == 0:
				frappe.db.set_value("Stock Entry", self.get('name'),"stock_status","Open")
			elif item.delivered_qty > 0 and item.delivered_qty < item.qty:
				frappe.db.set_value("Stock Entry", self.get('name'),"stock_status","Partial")
			elif item.delivered_qty == item.qty:
				frappe.db.set_value("Stock Entry", self.get('name'),"stock_status","Completed")


"""Validation for which Stock Entries to be allowed to be cancelled"""
def cancel_validation(self):
	if self.is_scrap == 1 and self.stock_returned == 1:
		frappe.throw("Cannot be cancelled as this a Reversed Stock Entry")
	elif self.stock_status == "Closed":
		frappe.throw("Stock Entry is already closed, It cannot be Cancelled")
	elif self.stock_status == "Expired":
		frappe.throw("Stock Entry is already Expired, It cannot be Cancelled")
