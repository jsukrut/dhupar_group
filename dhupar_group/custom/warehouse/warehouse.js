frappe.ui.form.on("Warehouse", {
	refresh: function(frm){
		frm.trigger('is_group')
	},

	is_group: function(frm){
		frm.toggle_display("reservation_warehouse", frm.doc.is_group ? 0:1)
	}
});