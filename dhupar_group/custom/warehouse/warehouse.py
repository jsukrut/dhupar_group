from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

def validate(doc,method):
	is_group_count_warehouse(doc)

@frappe.whitelist()
def is_group_count_warehouse(doc):
	"""In Warehouse more than is_group check not allowed"""
	
	is_group_count = frappe.db.sql("""select count(is_group) as group_count from `tabWarehouse` 
		where parent_warehouse='{0}' """.format(doc.parent_warehouse),as_dict= True)[0].get('group_count')
	if doc.is_group:
		if is_group_count > 1:
			frappe.throw("In Warehouse more than one <b>Is Group</b> is Not Allowed")