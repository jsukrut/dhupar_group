// Copyright (c) 2019, New Indictrans  and contributors
// For license information, please see license.txt

frappe.ui.form.on('Reservation Configurations', {
	refresh: function(frm){
		frm.trigger('querys_and_filters')
	},
	validate:function(frm){
		return new Promise(function(resolve, reject) {
			frappe.confirm(
				'Do you want to continue with the changes made in the configuration page?',
				function(){
					var negative = 'frappe.validated = false';
					resolve(negative);
					frappe.msgprint("Changes Saved Successfully.")
				},function(){
					reject();
					window.location.reload();
				});
		})
	},

	def_reservation_warehouse: function(frm){
		frm.trigger('querys_and_filters')
	},

	querys_and_filters:function(frm){
		frm.set_query("def_reservation_warehouse", function() {
			var res_warehouse = []
			frm.doc.warehouses.forEach(function(row) {
				if (row.reserved_warehouse){
					res_warehouse.push(row.reserved_warehouse)
				}
			});
			
		    return {
		    		"query": "dhupar_group.dhupar_group.doctype.reservation_configurations.reservation_configurations.default_res_warehouse",
					"filters":{
						is_group: 0,
						'res_warehouses': res_warehouse	
					}
				}
		});		
	},
	otfr:function(frm){
		if (frm.doc.disable_scheduler == 0){
			frappe.call({
				method: "dhupar_group.dhupar_group.doctype.reservation_schedule.reservation_schedule.reservation_shedular",
			});
		}else{
			frappe.msgprint("Scheduler Are Disabled")
		}
			
	},
	cancelled_dn:function(frm){
		if (frm.doc.disable_scheduler == 0){
			frappe.call({
				method: "dhupar_group.dhupar_group.doctype.reservation_schedule.reservation_schedule.cancelled_dn_details",
			});
		}else{
			frappe.msgprint("Scheduler Are Disabled")
		}
	},
	stock_entry_cancel:function(frm){
		if (frm.doc.disable_scheduler == 0){
			frappe.call({
				method: "dhupar_group.dhupar_group.doctype.reservation_schedule.reservation_schedule.reverse_expired_se_qty",
			});
		}else{
			frappe.msgprint("Scheduler Are Disabled")
		}
	},
	reservation_close:function(frm){
		if (frm.doc.disable_scheduler == 0){
			frappe.call({
				method: "dhupar_group.dhupar_group.doctype.reservation_schedule.reservation_schedule.get_closed_reservation",
			});
		}else{
			frappe.msgprint("Scheduler Are Disabled")
		}
	},
	emergency_se:function(frm){
		if (frm.doc.disable_scheduler == 0){
			frappe.call({
				method: "dhupar_group.dhupar_group.doctype.reservation_schedule.reservation_schedule.cancelled_ese_details",
			});
		}else{
			frappe.msgprint("Scheduler Are Disabled")
		}
	}

});

frappe.ui.form.on("Warehouse Configurations",{
	group_warehouse:function(frm,cdt,cdn){
		var data = locals[cdt][cdn]
		if(data.group_warehouse){
				var Test = ""
				frappe.model.set_value(data.doctype, data.name, "reserved_warehouse", Test);
			}
	},

});

cur_frm.fields_dict['warehouses'].grid.get_field("reserved_warehouse").get_query = function(doc, cdt, cdn) {
	var data = locals[cdt][cdn]
	if(data.group_warehouse){
		return {
			query : "dhupar_group.dhupar_group.doctype.reservation_configurations.reservation_configurations.filter_reserved_warehouse",
			filters: {
				"parent_warehouse":data.group_warehouse,

			}
		}
	}
}

cur_frm.fields_dict['warehouses'].grid.get_field("group_warehouse").get_query = function(doc, cdt, cdn) {
	var group_list = []
	for(var j = 0 ; j < cur_frm.doc.warehouses.length ; j++){
		if(cur_frm.doc.warehouses[j].group_warehouse){
			group_list.push(cur_frm.doc.warehouses[j].group_warehouse);
		}
	}
	return {
		filters: [
			['Warehouse', 'name', 'not in', group_list],
			['Warehouse', 'is_group','=',1],
			['Warehouse', 'parent_warehouse', '=', '']
		]
	}
}

