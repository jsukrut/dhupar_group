# -*- coding: utf-8 -*-
# Copyright (c) 2019, New Indictrans  and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class ReservationConfigurations(Document):
	 
	 def validate(self):
	 	no_of_days(self)
	 	warehouse_validation(self)
	 	reserved_warehouse_checkbox(self)

""" Set warehouse as reserved warehouse"""
@frappe.whitelist()
def reserved_warehouse_checkbox(self):
	warehouse_name_list = [row.reserved_warehouse for row in self.warehouses]

	frappe.db.sql(""" UPDATE `tabWarehouse` set reservation_warehouse = 0 where is_group = 0 """)
	
	if (len(warehouse_name_list)) > 1:
		frappe.db.sql("""UPDATE `tabWarehouse` set reservation_warehouse = 1 where is_group = 0 and name IN {0}""".
			format(tuple(list(set(warehouse_name_list)))))
	elif (len(warehouse_name_list)) <= 1:
		for ware in self.warehouses:
			frappe.db.sql("""UPDATE `tabWarehouse` set reservation_warehouse = 1 where is_group = 0 and name = '{0}'""".format(ware.reserved_warehouse))
	
@frappe.whitelist()
def filter_group_warehouse(doctype, txt, searchfield, start, page_len, filters):
	"""Only Group warehouse filter"""
	
	res=frappe.db.get_value("Warehouse", {'is_group': 1}, "name")
	return res
	
	
@frappe.whitelist()
def filter_reserved_warehouse(doctype, txt, searchfield, start, page_len, filters):
	"""Group warehouse against reserved warehouse filter"""
	
	if filters.get('parent_warehouse'):
		res_warehouse = frappe.db.sql("""SELECT name from `tabWarehouse`
			 where (parent_warehouse = '{parent_warehouse}'  and is_group = 0) and  name like '{txt}'""".format(parent_warehouse = filters.get('parent_warehouse'),txt= "%%%s%%" % txt));
		
		return res_warehouse
	else :
		return []	

@frappe.whitelist()
def default_res_warehouse(doctype, txt, searchfield, start, page_len, filters):
	"""defult reserved warehouse show only reserved warehouse on excluding warehouse configuration table"""
	
	if filters['res_warehouses']:
		excluding_res_warehouse = filters.get('res_warehouses')
		excluding_res_warehouse = "(" + ','.join(["'{0}'".format(p) for p in excluding_res_warehouse]) + ")"

		data = frappe.db.sql("""SELECT name from `tabWarehouse` 
			where is_group = 0 and name not in {0} and name like %(txt)s""".format(excluding_res_warehouse),
			{'txt': "%%%s%%" % frappe.db.escape(txt)}, debug = True);
		
		return data
	else:
		all_warehouse = frappe.db.sql("""SELECT name from `tabWarehouse` 
			where is_group = 0""");

		return all_warehouse

@frappe.whitelist()
def no_of_days(self):
	if self.reserved_days <= 0:
		frappe.throw("0 or Negative value not allowed")

@frappe.whitelist()
def warehouse_validation(self):
	for ware in self.warehouses:
		if ware.reserved_warehouse == self.def_reservation_warehouse:
			frappe.throw("Default Reservation warehouse cannot be same as Reserved Warehouse in Warehouse list.")
