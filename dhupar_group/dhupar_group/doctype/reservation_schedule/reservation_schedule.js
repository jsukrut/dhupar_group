// Copyright (c) 2019, New Indictrans  and contributors
// For license information, please see license.txt

frappe.ui.form.on('Reservation Schedule', {
	refresh: function(frm) {
		frm.trigger('reservation_type_check_func')
		frm.trigger('add_custom_button_make')
		frm.trigger('add_close_button')
		frm.trigger('so_ref_no_function')

	},

	validate: function(frm){
		frm.trigger('current_date_func')

	},

	before_submit: function(frm){
		frm.trigger('reservation_schedule_ref')
	},
	
	current_date_func: function(frm){
		var today_date = frappe.datetime.add_months(frappe.datetime.now())
		frm.set_value("date", today_date);
	},

	customer_reservation_type: function(frm){
		frm.trigger('reservation_type_check_func')
	},

	reservation_type_check_func:function(frm,row){
		if(frm.doc.customer_reservation_type == "One Time In Full Reservation (OTFR)"){
				var qty = frappe.meta.get_docfield('Reservation Schedule Item', "qty", frm.doc.name)
				qty.read_only = 1;

				fields_edi = frappe.get_meta("Item").fields.filter(function(field, idx) {
				return fields_edi.read_only == 1
				frm.refresh_fields();
			})
			}else if(frm.doc.customer_reservation_type == "Partial As per Available Reservation (PAAR)") {
				fields_editable = frappe.get_meta("Item").fields.filter(function(field, idx) {
					return fields_editable.read_only == 0
				})
				frm.refresh_fields();
				
				fields_edit = frappe.get_meta("Item").fields.filter(function(field, idx) {
					return fields_edit.read_only == 1
				})

				var qty = frappe.meta.get_docfield('Reservation Schedule Item', "qty", frm.doc.name)
				qty.read_only = 0;
				frm.refresh_fields();

			}

	},


	add_custom_button_make: function(frm){
		if((frm.doc.docstatus == 1) && (frm.doc.reservation_status != 'Closed')) {
			frm.add_custom_button(__("Delivery Note"),function() {
				frappe.model.open_mapped_doc({
					method: "dhupar_group.dhupar_group.doctype.reservation_schedule.reservation_schedule.make_delivery_note", 
					frm: cur_frm
				})
			},"Make")
			frm.page.set_inner_btn_group_as_primary(__("Make"));
		};

	},

	add_close_button: function(frm){
		if((frm.doc.docstatus == 1) && (frm.doc.reservation_status != 'Closed') && (frm.doc.customer_reservation_type == "Partial As per Available Reservation (PAAR)")){
			frm.add_custom_button(__('Close'), function () {
				frappe.call({
					method: "dhupar_group.dhupar_group.doctype.reservation_schedule.reservation_schedule.update_close_status",
					args: {
						name : frm.doc.name
					},
				});
				window.location.reload();
			});
		} 
	},
	
	so_ref_no_function: function(frm,row){
			var  so_ref_no = frm.doc.so_reference_no
			$.each(frm.doc.items,function(idx,data){
				if(frm.doc.so_reference_no){
					frappe.model.set_value(data.doctype, data.name, "against_sales_order", so_ref_no);
				}
			})
	},

	reservation_schedule_ref: function(frm,row){
		var  res_schedule_ref_no = frm.doc.name
			$.each(frm.doc.items,function(idx,data){
				if(frm.doc.so_reference_no){
					frappe.model.set_value(data.doctype, data.name, "against_res_schedule_no", res_schedule_ref_no);
				}
			})

	}

});

frappe.ui.form.on("Reservation Schedule Item",{
	item_code:function(frm,cdt,cdn){
		var data = locals[cdt][cdn]
		frm.events.so_ref_no_function(frm, data)	
		frm.events.reservation_schedule_ref(frm, data)
	},
	qty:function(frm,cdt,cdn){
		var data = locals[cdt][cdn]
		var amount = data.qty * data.rate
		frappe.model.set_value(cdt, cdn, "amount", amount);
	}
});