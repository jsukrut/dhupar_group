# -*- coding: utf-8 -*-
# Copyright (c) 2019, New Indictrans  and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.model.mapper import get_mapped_doc
from frappe.utils import nowdate, cstr, flt, cint, now, getdate,get_datetime,time_diff_in_seconds,add_to_date,time_diff_in_seconds,add_days
from datetime import datetime
import time
from datetime import date, timedelta


class ReservationSchedule(Document):
	def validate(self):
		set_remaining_qty(self)
		reserve_qty_validation(self)

	def on_submit(self):
		make_stock_entry(self)
		check_qty_available(self)

	def on_update_after_submit(self):
		update_reservation_status(self)

	def on_cancel(self):
		update_cancel_qty(self)
		pass

@frappe.whitelist()
def make_delivery_note(source_name, target_doc=None):
	
	def postprocess(source, doc):
		doc.docstatus = 0

	def update_item(source, target,source_name):
		# qty is for packed items, because packed items don't have stock_qty field
		target.warehouse = source_name.reserved_warehouse
		target.allowed_qty = source.get("reserved_qty")
		# target.rate = source.get("rate")
	
	target_doc = get_mapped_doc("Reservation Schedule", source_name,
		{
		"Reservation Schedule": {
			"doctype" : "Delivery Note",
			"field_map" : {
				"name" : "reservation_schedule_ref_no",
				"so_reference_no": "sales_order_reference_no",
				"customer" : "customer",
				"reserved_warehouse" : "set_warehouse"
			}
		},
		"Reservation Schedule Item": {
			"doctype" : "Delivery Note Item",
			"field_map" : {
				"delivery_date": "delivery_date",
				"against_sales_order" : "against_sales_order",
				"against_res_schedule_no" : "against_reservation_schedule",
				"reserved_qty" : "qty",
				"name":"reservation_schedule_item_name",
				"sales_order_item":"so_detail"
			},"postprocess":update_item

		},
	}, target_doc, postprocess)
	return target_doc

""" If quantity is 0 or less than 0"""
@frappe.whitelist()
def reserve_qty_validation(self):
	for item in self.items:
		if item.qty <= 0:
			frappe.throw("Quantity can not be 0 or less than 0")


""" For Stock reservation"""
@frappe.whitelist()
def make_stock_entry(self):
	try:
		reservation_item = []
		for item in self.items:
			stock = has_first_stock(item.item_code,self.group_warehouse)
			if stock:
				reserved_qty_from_wr = [reserve['qty'] for reserve in reservation_item if reserve['item_code'] == item.get('item_code') and reserve['s_warehouse'] == stock.get('warehouse')]
				if sum(reserved_qty_from_wr) < flt(stock.get("actual_qty")):
					if stock.get('actual_qty') >= item.remaining_qty:
						if item.remaining_qty > 0:
							temp = {
								"item_code": item.get("item_code"),
								"item_name": item.get("item_name"),
								"qty":item.get("remaining_qty"),
								"uom":item.get("uom"),
								"s_warehouse": stock.get('warehouse'),
								"t_warehouse": self.reserved_warehouse,
								"reservation_schedule_item":item.get('name'),
								"sales_order_item": item.get('sales_order_item')
							}
							reservation_item.append(temp)

					else:
						total_stock = has_stock(item.item_code,self.group_warehouse)
						reserved_qty = 0.0
						if total_stock:
							for stock in total_stock:
								reserved_qty_from_wr = [reserve['qty'] for reserve in reservation_item if reserve['item_code'] == item.get('item_code') and reserve['s_warehouse'] == stock.get('warehouse')]
								if sum(reserved_qty_from_wr) < flt(stock.get("actual_qty")):
									if reserved_qty <= item.remaining_qty:
										if (item.remaining_qty - reserved_qty) > flt(stock.get("actual_qty")):
											temp = {
												"item_code": item.get("item_code"),
												"item_name": item.get("item_name"),
												"qty":stock.get("actual_qty"),
												"uom":item.get("uom"),
												"s_warehouse": stock.get('warehouse'),
												"t_warehouse": self.reserved_warehouse,
												"reservation_schedule_item":item.get('name'),
												"sales_order_item": item.get('sales_order_item')
											}
											reserved_qty = reserved_qty + flt(stock.get("actual_qty"))
											reservation_item.append(temp)
										elif item.remaining_qty - reserved_qty < stock.get("actual_qty"):
											temp = {
												"item_code": item.get("item_code"),
												"item_name": item.get("item_name"),
												"qty":item.get('remaining_qty') - reserved_qty,
												"uom":item.get("uom"),
												"s_warehouse": stock.get('warehouse'),
												"t_warehouse": self.reserved_warehouse,
												"reservation_schedule_item":item.get('name'),
												"sales_order_item": item.get('sales_order_item')
											}
											reserved_qty = reserved_qty +  flt(stock.get("actual_qty"))
											reservation_item.append(temp)
								else:
									pass
				else:
					pass
		if reservation_item:
			reserve_stock(self,reservation_item)
	except Exception as e:
		make_error_log(title="Make stock Entry",method="make_stock_entry",
			data = str(reservation_item),
			traceback=frappe.get_traceback(),exception=True)
		raise e

""" Get warehouse as has first stock with FIFO order"""
@frappe.whitelist()
def has_first_stock(item_code,group_warehouse):
	if item_code and group_warehouse:
		has_first_stock = frappe.db.sql("""SELECT b.item_code,b.modified,b.warehouse,b.actual_qty
				from `tabBin` b,`tabWarehouse` w  
				where (b.item_code ='{0}' and b.warehouse = w.name) and 
				(b.actual_qty > 0 and w.parent_warehouse ='{1}' and w.reservation_warehouse = 0) order by b.modified asc limit 1""".format(item_code,group_warehouse),as_dict = 1)
		if has_first_stock:
			return has_first_stock[0]

""" Get warehouses which has stock with order"""
@frappe.whitelist()
def has_stock(item_code,group_warehouse):
	if item_code and group_warehouse:
		has_stock = frappe.db.sql("""SELECT b.item_code,b.modified,b.warehouse,b.actual_qty
				from `tabBin` b,`tabWarehouse` w  
				where (b.item_code ='{0}' and b.warehouse = w.name) and 
				(b.actual_qty > 0 and w.parent_warehouse ='{1}' and w.reservation_warehouse = 0) order by b.modified asc """.format(item_code,group_warehouse),as_dict = 1)
		return has_stock


""" Get warehouses which has stock with order"""
@frappe.whitelist()
def total_item_stock(item_code,group_warehouse):
	if item_code and group_warehouse:
		total_stock = frappe.db.sql("""SELECT b.item_code,sum(b.actual_qty) as total_stock
				from `tabBin` b,`tabWarehouse` w  
				where (b.item_code ='{0}' and b.warehouse = w.name) and 
				(b.actual_qty > 0 and w.parent_warehouse ='{1}' and w.reservation_warehouse = 0) """.format(item_code,group_warehouse),as_dict = 1)
		if total_stock:
			return total_stock[0]


""" Make Stock Entry for stock Reservation"""
@frappe.whitelist()
def reserve_stock(self,reservation_item):
	try:
		current_date = frappe.utils.today()
		stock_entry = frappe.new_doc("Stock Entry")
		if stock_entry:
			stock_entry.posting_date = current_date
			stock_entry.reservation_schedule_ref_no = self.get('name')
			stock_entry.purpose = "Material Transfer"
			for item in reservation_item:
				stock_entry.append("items",{
					"item_code": item.get("item_code"),
					"item_name": item.get("item_name"),
					"qty":item.get("qty"),
					"uom":item.get("uom"),
					"s_warehouse": item.get('s_warehouse'),
					"t_warehouse": item.get('t_warehouse'),
					"against_reservation_schedule" : self.name,
					"reservation_schedule_item":item.get('reservation_schedule_item')	
					})
			stock_entry.last_reservation_date = add_days(stock_entry.posting_date, self.reserved_days)
			stock_entry.save(ignore_permissions = True)
			stock_entry.submit()
			frappe.db.commit()
			update_remaining_qty(stock_entry.get('name'),self.get('name'))
	except Exception as e:
		make_error_log(title="Reserve Stock Error",method="reserve_stock",
			data = reservation_item,
			traceback=frappe.get_traceback(),exception=True)
		raise e

""" Set Remaining QTY for PAAR"""
@frappe.whitelist()
def set_remaining_qty(self):
	if self.docstatus == 0:
		for item in self.items:
			item.remaining_qty = item.qty

""" Update Remaining QTY for PAAR"""
@frappe.whitelist()
def update_remaining_qty(stock_entry_name,reservation_shedular_name):
	try:
		if stock_entry_name:
			se_doc = frappe.get_doc("Stock Entry", stock_entry_name)
			res_doc = frappe.get_doc("Reservation Schedule", reservation_shedular_name)
			for res_item in res_doc.items:
				for se_item in se_doc.items:
					if res_item.item_code == se_item.item_code and res_item.name == se_item.reservation_schedule_item:
						res_item.remaining_qty = res_item.remaining_qty - se_item.qty
						res_item.reserved_qty = res_item.reserved_qty + se_item.qty
			res_doc.save()
			reservation_name_on_sales_order(res_doc.name)
	except Exception as e:
		make_error_log(title="Update Remaining Quantity",method="update_remaining_qty",
			data = stock_entry_name,
			traceback=frappe.get_traceback(),exception=True)
		raise e
""" Update Remaining Status"""
@frappe.whitelist()
def update_reservation_status(self):
	if self.reservation_status != "Closed":
		rem_qty = 0.0
		total_qty = 0.0
		deli_qty = 0.0
		for item in self.items:
			rem_qty = rem_qty + item.remaining_qty
			total_qty = total_qty + item.qty
			deli_qty = deli_qty + item.delivered_qty
		if rem_qty > 0 and rem_qty < total_qty:
			frappe.db.set_value("Reservation Schedule", self.get('name'), "reservation_status", "Partial")
		elif rem_qty == 0 and deli_qty == 0:
			frappe.db.set_value("Reservation Schedule", self.get('name'), "reservation_status", "Ready for Dispatch")
		elif rem_qty == 0 and deli_qty > 0 and deli_qty < total_qty:
			frappe.db.set_value("Reservation Schedule", self.get('name'), "reservation_status", "Partially Delivered")
		elif rem_qty > 0 and deli_qty > 0 and deli_qty < total_qty:
			frappe.db.set_value("Reservation Schedule", self.get('name'), "reservation_status", "Partially Delivered")
		elif rem_qty == 0 and deli_qty == total_qty:
			frappe.db.set_value("Reservation Schedule", self.get('name'), "reservation_status", "Completed")
		elif rem_qty == total_qty:
			frappe.db.set_value("Reservation Schedule", self.get('name'), "reservation_status", "Open")
	reservation_name_on_sales_order(self.get('name'))

"""Check allowed quantity for PAAR """
@frappe.whitelist()
def check_qty_available(self):
	try:
		if self.so_reference_no and self.customer_reservation_type == "Partial As per Available Reservation (PAAR)":
			se_doc = frappe.get_doc("Sales Order", self.so_reference_no)
			for item in self.items:
				for se_item in se_doc.items:
					if se_item.item_code == item.item_code and se_item.name == item.sales_order_item:
						se_item.reservation_schedule_qty = se_item.reservation_schedule_qty - item.qty
			se_doc.save()

			for item in self.items:
				if item.qty > item.reservation_schedule_qty:
					frappe.throw("The Quantity to be Reserved for Item '{0}' is greater than the quantity of sales order. You can reserve maximum upto {1} quantity for this Item.".format(item.item_code,item.reservation_schedule_qty))
	except Exception as e:
		make_error_log(title="Failed Reservation shedular OFTR",method="reservation_shedular",
			data = "",
			traceback=frappe.get_traceback(),exception=True)
		raise e

""" Update sales order qty on cancel of rs"""
@frappe.whitelist()
def update_cancel_qty(self):
	if self.so_reference_no:
		se_doc = frappe.get_doc("Sales Order", self.so_reference_no)
		for item in self.items:
			for se_item in se_doc.items:
				if se_item.item_code == item.item_code and se_item.name == item.sales_order_item:
					se_item.reservation_schedule_qty = se_item.reservation_schedule_qty + item.qty
		se_doc.save()

""" Update status to close on close button """
@frappe.whitelist()
def update_close_status(name):
	frappe.db.set_value("Reservation Schedule",{"name":name},"reservation_status","Closed")
	update_so_qty_for_rs_close(name)

""" Update Sales order quantity on closure of rs """
def update_so_qty_for_rs_close(name):
	re_doc = frappe.get_doc("Reservation Schedule",{"name":name})
	if re_doc.reservation_status == 'Closed':
		sales_doc = frappe.get_doc("Sales Order",{"name":re_doc.so_reference_no})
		for row in sales_doc.items:
			for item in re_doc.items:
				if row.item_code == item.item_code and row.name == item.sales_order_item:
					row.reservation_schedule_qty = row.reservation_schedule_qty + (flt(item.qty) - flt(item.delivered_qty))
		for rs in sales_doc.reservation_references:
			if rs.reservation_schedule_no == re_doc.name:
				rs.status = re_doc.reservation_status
		sales_doc.save()

"""Error Log"""
@frappe.whitelist()
def make_error_log(title,method,data,traceback,exception):
	if exception:
		error_doc = frappe.new_doc("Error Log")
		if error_doc:
			error_doc.title = title
			error_doc.method = method
			error_doc.data = data
			error_doc.traceback = traceback
			error_doc.save(ignore_permissions =1)

""" Make Shedular for OTFR and PAAR"""
@frappe.whitelist()
def reservation_shedular():
	disable_scheduler = frappe.db.get_singles_value("Reservation Configurations","disable_scheduler")
	if disable_scheduler == 0:
		try:
			open_reservation_shedule = frappe.db.sql("""
				SELECT name from `tabReservation Schedule` where reservation_status in ("Open","Partial","Partially Delivered","Ready for Dispatch") and is_expired = 0 and docstatus = 1 order by 
				creation asc """,as_dict=1)
			if open_reservation_shedule:
				for reservation in open_reservation_shedule:
					res_doc = frappe.get_doc("Reservation Schedule", reservation.get('name'))
					if res_doc:
						make_stock_entry(res_doc)
		except Exception as e:
			make_error_log(title="Failed Reservation shedular OFTR",method="reservation_shedular",
				data = res_doc,
				traceback=frappe.get_traceback(),exception=True)
			raise e
	else:
		pass

""" Scheduler for Cancelled Dn """
@frappe.whitelist()
def cancelled_dn_details():
	disable_scheduler = frappe.db.get_singles_value("Reservation Configurations","disable_scheduler")
	if disable_scheduler == 0:
		try:
			reservation_item = []
			delivery_list = []
			delivery_note = frappe.db.sql("""
					SELECT name from `tabDelivery Note` where reservation_schedule_ref_no is NOT NULL and status = 'Cancelled' and stock_return = 0
				""",as_dict=1)

			emergency_delivery_note = frappe.db.sql("""
					SELECT name from `tabDelivery Note` where reservation_schedule_ref_no is NULL and status = 'Cancelled' and stock_return = 0 and emergency_dispatch = 1
				""",as_dict=1)

			if delivery_note:
				for delivery in delivery_note:
					get_dn_doc = frappe.get_doc("Delivery Note",delivery.get('name'))
					for item in get_dn_doc.pick_up_list:
						temp = {
							"item_code": item.get("item_code"),
							"quantity":item.get('quantity'),
							"source_warehouse": item.get("target_warehouse"),
							"target_warehouse": item.get("source_warehouse"),
							"uom":item.get("uom"),
							"item_name":item.get("item_name")
							}
						reservation_item.append(temp)
						dn_reference = {
								"delivery_name" : delivery.name
							}
						delivery_list.append(dn_reference)

			elif emergency_delivery_note:
				for delivery in emergency_delivery_note:
					get_dn_doc = frappe.get_doc("Delivery Note",delivery.get('name'))
					for item in get_dn_doc.pick_up_list:
						temp = {
							"item_code": item.get("item_code"),
							"quantity":item.get('quantity'),
							"source_warehouse": item.get("target_warehouse"),
							"target_warehouse": item.get("source_warehouse"),
							"uom":item.get("uom"),
							"item_name":item.get("item_name")
							}
						reservation_item.append(temp)
						dn_reference = {
							"delivery_name" : delivery.name
							}
						delivery_list.append(dn_reference)


			if reservation_item:
				return_dn_stock(reservation_item)
				for dn in delivery_list:
					update_dn_return(dn.get('delivery_name'))
		except Exception as e:
			make_error_log(title="Cancelled Delivery Note",method="cancelled_dn_details",
				data = "",
				traceback=frappe.get_traceback(),exception=True)
			raise e
	else:
		pass


@frappe.whitelist()
def return_dn_stock(reservation_item):
	try:
		current_date = frappe.utils.today()
		stock_entry = frappe.new_doc("Stock Entry")
		if stock_entry:
			stock_entry.posting_date = current_date
			stock_entry.purpose = "Material Transfer"
			stock_entry.stock_return_dn = 1
			for item in reservation_item:
				stock_entry.append("items",{
					"item_code": item.get("item_code"),
					"item_name": item.get("item_name"),
					"qty":item.get("quantity"),
					"uom":item.get("uom"),
					"s_warehouse": item.get('source_warehouse'),
					"t_warehouse": item.get('target_warehouse'),
				})
			stock_entry.save(ignore_permissions = True)
			stock_entry.submit()
	except Exception as e:
		make_error_log(title="Return DN Stock",method="return_dn_stock",
			data = stock_entry.name,
			traceback=frappe.get_traceback(),exception=True)
		raise e

@frappe.whitelist()
def update_dn_return(name):
	frappe.db.set_value("Delivery Note",{"name":name},"stock_return",1)
	frappe.db.commit()

""" Scheduler for Partially used or delivered Stock Entries"""
@frappe.whitelist()
def reverse_expired_se_qty():
	disable_scheduler = frappe.db.get_singles_value("Reservation Configurations","disable_scheduler")
	if disable_scheduler == 0:
		try:
			stock_item = []
			stock_entry_list = []
			stock_entry_doc = frappe.db.sql("""
					SELECT name from `tabStock Entry` where reservation_schedule_ref_no is NOT NULL and last_reservation_date < CURDATE() and stock_status in ('Open','Partial') and docstatus = 1
				""",as_dict=1)
			
			if stock_entry_doc:
				for stock in stock_entry_doc:
					stock_doc_entries = frappe.get_doc("Stock Entry", stock.get('name'))
					for item in stock_doc_entries.items:
						if (flt(item.get('qty')) - flt(item.get('delivered_qty'))) > 0:
							temp = {
								"item_code": item.get("item_code"),
								"quantity":(flt(item.get('qty')) - flt(item.get('delivered_qty'))),
								"source_warehouse": item.get("t_warehouse"),
								"target_warehouse": item.get("s_warehouse"),
								"uom":item.get("uom"),
								"item_name":item.get("item_name")
								}
							stock_item.append(temp)
							se_ref = {
								"se_name": stock.name
							}
							stock_entry_list.append(se_ref)
			if stock_item:
				create_stock(stock_item)
				for se in stock_entry_list:
					update_stock_entry_status(se.get('se_name'))
		except Exception as e:
			make_error_log(title="Reverse Expired Stock Entry",method="reverse_expired_se_qty",
				data = "",
				traceback=frappe.get_traceback(),exception=True)
			raise e
	else:
		pass

@frappe.whitelist()
def create_stock(stock_item):
	try:
		current_date = frappe.utils.today()
		stock_entry = frappe.new_doc("Stock Entry")
		if stock_entry:
			stock_entry.posting_date = current_date
			stock_entry.purpose = "Material Transfer"
			for item in stock_item:
				stock_entry.append("items",{
					"item_code": item.get("item_code"),
					"item_name": item.get("item_name"),
					"qty":item.get("quantity"),
					"uom":item.get("uom"),
					"s_warehouse": item.get('source_warehouse'),
					"t_warehouse": item.get('target_warehouse'),
				})
			stock_entry.stock_returned = 1
			stock_entry.save(ignore_permissions = True)
			stock_entry.submit()
	except Exception as e:
		make_error_log(title="create_stock",method="create_stock",
			data = stock_entry.name,
			traceback=frappe.get_traceback(),exception=True)
		raise e

@frappe.whitelist()
def update_stock_entry_status(name):
	frappe.db.set_value("Stock Entry",{"name":name},"stock_status","Expired")
	frappe.db.commit()


""" Scheduler to Update name and Status on Sales Order"""
@frappe.whitelist()
def reservation_name_on_sales_order(res):
	if res:
		try:
			rs_doc = frappe.get_doc('Reservation Schedule',res)
			so_doc = frappe.get_doc("Sales Order",{"name":rs_doc.so_reference_no})
			reservation_name_list = list(set([res.reservation_schedule_no for res in so_doc.reservation_references]))
			if so_doc:
				try:
					if rs_doc.name in reservation_name_list:
						for rs in so_doc.reservation_references:
							if rs.reservation_schedule_no == rs_doc.name:
								rs.status = rs_doc.reservation_status
					else:
						so_doc.append("reservation_references",{
							"reservation_schedule_no": rs_doc.name,
							"status": rs_doc.reservation_status
							})
				except Exception as e:
					raise e
			so_doc.save()
		except Exception as e:
			make_error_log(title="Reservation Name on Sales Order",method="reservation_name_on_sales_order",
				data = res,
				traceback=frappe.get_traceback(),exception=True)
			raise e
	else:
		pass

""" Scheduler for stock return on closure of reservation Schedule"""
@frappe.whitelist()
def get_closed_reservation():
	disable_scheduler = frappe.db.get_singles_value("Reservation Configurations","disable_scheduler")
	if disable_scheduler == 0:
		try:
			stock_list = []
			se_list = []
			se_doc = frappe.db.sql("""
				SELECT se.name,rs.name as rs_name from `tabReservation Schedule` as rs, `tabStock Entry` as se where rs.reservation_status = 'Closed' and rs.is_expired = 0 and se.reservation_schedule_ref_no = rs.name and se.stock_status in ('Open','Partial') and se.docstatus = 1
				""",as_dict=1)

			if se_doc:
				for stock in se_doc:
					doc_stock_entry = frappe.get_doc("Stock Entry", stock.get('name'))
					for item in doc_stock_entry.items:
						remaining_qty = flt(item.get('qty')) - flt(item.get('delivered_qty'))
						if remaining_qty > 0:
							temp = {
								"item_code": item.get("item_code"),
								"quantity":remaining_qty,
								"source_warehouse": item.get("t_warehouse"),
								"target_warehouse": item.get("s_warehouse"),
								"uom":item.get("uom"),
								"item_name":item.get("item_name")
							}
							stock_list.append(temp)
							se_reference = {
								"stock_entry" : stock.name,
								"reservation_schedule" : stock.rs_name
							}
							se_list.append(se_reference)

			if stock_list:
				create_stock_doc(stock_list)
				for se in se_list:
					update_stock_status(se.get('stock_entry'))
					update_is_expired(se.get('reservation_schedule'))
		except Exception as e:
			make_error_log(title="Get closed Reservation",method="get_closed_reservation",
				data = "",
				traceback=frappe.get_traceback(),exception=True)
			raise e
	else:
		pass

@frappe.whitelist()
def create_stock_doc(stock_list):
	try:
		current_date = frappe.utils.today()
		stock_entry = frappe.new_doc("Stock Entry")
		if stock_entry:
			stock_entry.posting_date = current_date
			stock_entry.purpose = "Material Transfer"
			for item in stock_list:
				stock_entry.append("items",{
					"item_code": item.get("item_code"),
					"item_name": item.get("item_name"),
					"qty":item.get("quantity"),
					"uom":item.get("uom"),
					"s_warehouse": item.get('source_warehouse'),
					"t_warehouse": item.get('target_warehouse'),
				})
			stock_entry.stock_returned = 1
			stock_entry.save(ignore_permissions = True)
			stock_entry.submit()
	except Exception as e:
		make_error_log(title="create_stock_doc",method="create_stock_doc",
			data = stock_entry.name,
			traceback=frappe.get_traceback(),exception=True)
		raise e


@frappe.whitelist()
def update_stock_status(name):
	frappe.db.set_value("Stock Entry",{"name":name},"stock_status","Closed")
	frappe.db.commit()

@frappe.whitelist()
def update_is_expired(name):
	if name:
		frappe.db.set_value("Reservation Schedule",{"name":name},"is_expired",1)
		frappe.db.set_value("Reservation Schedule",{"name":name},"reservation_status","Closed")
		frappe.db.commit()

""" Scheduler for Cancelled ESE(Emergency Stock Entry) """
@frappe.whitelist()
def cancelled_ese_details():
	disable_scheduler = frappe.db.get_singles_value("Reservation Configurations","disable_scheduler")
	if disable_scheduler == 0:
		try:
			emergency_item = []
			emergency_stock_list = []
			emergency_stock_entry = frappe.db.sql("""SELECT name 
				from `tabStock Entry` 
				where is_scrap = 1 and is_delivered = 0 and is_used = 0 and stock_returned = 0 and stock_return_dn = 0 and docstatus = 2 """,as_dict=1)
			if emergency_stock_entry:
				for stock_entry in emergency_stock_entry:
					se_doc = frappe.get_doc("Stock Entry",stock_entry.get('name'))
					for item in se_doc.reservation_table:
						temp = {
							"source_warehouse":item.get("target_warehouse"),
							"item_code": item.get("item_code"),
							"item_name":item.get("item_name"),
							"quantity":item.get('quantity'),
							"target_warehouse": item.get("source_warehouse"),
							"uom":item.get("uom"),
							"reservation_schedule":item.get("reservation_schedule")
							}
						emergency_item.append(temp)
						emergency = {
							"stock_entry" : stock_entry.name
						}
						emergency_stock_list.append(emergency)
			if emergency_item:
				return_stock(emergency_item,stock_entry.name)
				for emergency in emergency_stock_list:
					update_se_status(emergency.get('stock_entry'))
		except Exception as e:
			make_error_log(title="Close Emergency Stock Entry",method="cancelled_ese_details",
				data = "",
				traceback=frappe.get_traceback(),exception=True)
			raise e
	else:
		pass

@frappe.whitelist()
def return_stock(emergency_item,name):
	try:
		current_date = frappe.utils.today()
		stock_entry = frappe.new_doc("Stock Entry")
		if stock_entry:
			stock_entry.posting_date = current_date
			stock_entry.purpose = "Material Transfer"
			for item in emergency_item:
				stock_entry.append("items",{
					"item_code": item.get("item_code"),
					"item_name": item.get("item_name"),
					"qty":item.get("quantity"),
					"uom":item.get("uom"),
					"s_warehouse": item.get('source_warehouse'),
					"t_warehouse": item.get('target_warehouse'),
					"reservation_schedule":item.get("reservation_schedule")
				})
			stock_entry.stock_returned = 1
			stock_entry.save(ignore_permissions = True)
			stock_entry.submit()
	except Exception as e:
		make_error_log(title="Return Stock",method="return_stock",
			data = "",
			traceback=frappe.get_traceback(),exception=True)
		raise e

@frappe.whitelist()
def update_se_status(name):
	frappe.db.set_value("Stock Entry",{"name":name},"stock_returned",1)
	frappe.db.commit()